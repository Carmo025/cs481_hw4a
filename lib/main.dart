//Name: Carlitos Carmona
//Email: carmo025@cougars.csusm.edu
//Professor: Zampell
//Class: CS 481 T/TH 7:00pm-8:15pm
//HW4 - Animation
//Purpose - to demonstrate an animation repeatedly using an image of my choice.
//The image is captured from a character in the video game Call of Duty. The image
//uses fade out features to give the image animated effects.

/*Bitbucket
https://bitbucket.org/Carmo025/cs481_hw4a/src/master/
*/

// Mathematical constants and functions
import 'dart:math';

//include animation and material libraries
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

//runs my main program at a minimum
void main() => runApp(LogoApp());

//Create a class with a container including the animation,size, opacity
//Using animations utilizes states
class AnimatedLogo extends AnimatedWidget
{
  //Two animation keys including the size and opacity
  //Before add listener , a dart feature known as cascading
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

// Root of my application
  //Build function
  Widget build(BuildContext context)
  {
    final animation = listenable as Animation<double>;
    //Centers the image/logo
    return Center
      (
      child: Opacity
        (
        //Changes on the left opacity, the image will FADE OUT
        //with the size of the animation/image
        opacity: _opacityTween.evaluate(animation),
        child: Container
          (
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          //I chose to implement my own image
          child: Image.asset(
            // IMPLEMENTS IMAGE FROM DIRECTORY - images/CoD.jpg
            'images/CoD.jpg',
            //FITS THE IMAGE INTO THE animation DESIGN
            width: 600,
            height: 300,
            fit: BoxFit.cover,

          ),
        ),
      ),
    );
  }
}

//Instantiate the class
class LogoApp extends StatefulWidget
{
  _LogoAppState createState() => _LogoAppState();
}
//"They had us in the First Half"
//This becomes the start of my widget

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin
{
  Animation<double> animation; // animation
  AnimationController controller; //control

//Create the init state
  @override
  void initState()
  {
    super.initState();

    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    // animation controller tweens
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
    //The states that have altered here in the animations object value
      ..addStatusListener((status)
      {
        //if and else statements used to control the animation opacity
        //and direction of image
        if (status == AnimationStatus.completed)
        {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed)
        {
          //Similar to an iterator
          controller.forward();
        }
      });
    //Similar to an iterator, advances from 0-300
    controller.forward();
  }

  //Build
  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  //Implement dispose working like a deconstructor before the app closes
  //prevents memory leaks
  //from occuring while the app is running

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}// END




